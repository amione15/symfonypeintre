# MonProjetPeintre

est un site intezrnet presentant les oeuvres d'un peintre

## Environnement de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* DOcker
* Docker-compose

Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante (de la CLI Symfony) :

```bash
symfony check:requirements
```

### Lancer l'environnement de développement
``